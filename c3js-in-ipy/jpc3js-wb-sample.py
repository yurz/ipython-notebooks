
# coding: utf-8

# In[19]:

import sys
import IPython
from IPython.display import display
from IPython.display import HTML, Javascript


# In[20]:

print(sys.version)
print(IPython.__version__)


# #### Download some GDP data from World Bank and prepare Pandas DataFrame object:

# In[21]:

from pandas.io import wb


# In[22]:

df = wb.download(indicator='NY.GDP.PCAP.PP.KD', country="all", start=2010, end=2014).unstack()


# #### Cleanup and create df_selected with top 5 and bottom 5 countries based on GDP Per Capita Growth between 2010 and 2014:

# In[23]:

df.columns = df.columns.levels[1]
df = df.dropna(how="any")
df = df.astype(int)
df["5yr_change"] = (df["2014"]-df["2010"])/df["2010"]
df = df.sort_values("5yr_change")
df_selected = df.head().append(df.tail())
df_selected


# #### Convert DataFrame to a format understood by c3:

# In[24]:

def df2c3(df):
    import numpy as np
    data = {}
    categories = list(df.index.values)
    columns = []
    for col in df.columns:
        this_col = [col]
        this_col.extend(list(df[col].values))
        columns.append(this_col)
    data["columns"] = columns
    data["categories"] = categories
    return data


# In[25]:

viz_data = df2c3(df_selected[['2010', '2011', '2012', '2013', '2014']].sort_values("2014", ascending=False).T)


# In[26]:

viz_data


# #### Load d3 and c3 JavaScript Libraries:

# In[27]:

get_ipython().run_cell_magic('javascript', '', 'require.config({\n   baseUrl: "",\n   paths: { \n            d3: \'//cdnjs.cloudflare.com/ajax/libs/d3/3.4.8/d3.min\',\n            c3: "//cdnjs.cloudflare.com/ajax/libs/c3/0.4.10/c3.min"\n          }\n});')


# In[28]:

get_ipython().run_cell_magic('javascript', '', 'require(["d3", "c3"], function(d3, c3) {\nwindow.d3 = d3;\nwindow.c3 = c3;\n});')


# #### Pass Python viz_data object to JavaScript:

# In[29]:

Javascript("""
           window.viz_data={viz_data};
           """.format(viz_data=viz_data))


# #### Add c3 styles and prepare div tag for chart:

# In[30]:

get_ipython().run_cell_magic('javascript', '', '$("head").append(\'<link href="https://cdnjs.cloudflare.com/ajax/libs/c3/0.4.10/c3.min.css" rel="stylesheet" />\');\n$("head").append(\'<style>.c3-line { stroke-width: 3px; }</style>\');')


# In[31]:

get_ipython().run_cell_magic('javascript', '', '$("#chart1").remove();\nelement.append("<h4>GDP Per Capita 2010-2014 Growth - Top 5 and Bottom 5 Countries Line Chart</h4><div id=\'chart1\'></div>");')


# #### Draw a Chart in the output cell above:

# In[32]:

get_ipython().run_cell_magic('javascript', '', 'var chart = c3.generate({\n    bindto: "#chart1",\n    size: {height: 480 },\n    data: {\n        columns: viz_data["columns"],\n        type:"spline"\n    },\n    axis: {\n            x: {\n                type:"category",\n                categories: viz_data["categories"]\n              }\n          },\n    grid: {\n            x: {show: true},\n            y: { show: true}\n        }       \n});')


# #### You should get something like this:

# <img style="float: right; margin-right: 60px;" src="line_chrt.gif">

# #### Now let's make a bar chart:

# In[33]:

get_ipython().run_cell_magic('javascript', '', '$("#chart2").remove();\nelement.append("<h4>GDP Per Capita 2010-2014 Growth - Top 5 and Bottom 5 Countries Bar Chart</h4><div id=\'chart2\'></div>");')


# In[34]:

get_ipython().run_cell_magic('javascript', '', 'var chart = c3.generate({\n    bindto: "#chart2",\n    size: {height: 640 },\n    data: {\n        columns: viz_data["columns"],\n        type:"bar"\n    },\n    axis: {\n            x: {\n                type:"category",\n                categories: viz_data["categories"]\n              }\n          },\n    grid: {\n            x: {show: true},\n            y: { show: true}\n        }       \n});')


# You can find more samples of c3.js charts here: http://c3js.org/examples.html

# [Yuri Zhylyuk](http://yznotes.com) 2016-04-30
